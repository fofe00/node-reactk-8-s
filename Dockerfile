FROM node:14.17.3
ADD . /app/
WORKDIR /app
RUN npm install

EXPOSE 3000


CMD npm run start